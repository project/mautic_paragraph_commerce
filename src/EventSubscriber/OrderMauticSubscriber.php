<?php

namespace Drupal\mautic_paragraph_commerce\EventSubscriber;

use Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Locale\CountryManager;

/**
 * Connect commerce order with Mautic.
 */
class OrderMauticSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The client IP address.
   *
   * @var string
   */
  protected $clientIp;

  /**
   * The host address.
   *
   * @var string
   */
  protected $host;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The commerce_wishlist_expiration queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The MauticCommerceApi service.
   *
   * @var \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface
   */
  protected $mauticCommerceApi;

  /**
   * Constructs a new Event Subscriber object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The corresponding request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface $mautic_paragraph_commerce_api
   *   The Mautic API service.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, ConfigFactoryInterface $config_factory, QueueFactory $queue_factory, MauticCommerceApiInterface $mautic_paragraph_commerce_api) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->clientIp = $request_stack->getCurrentRequest()->getClientIp();
    $this->host = $request_stack->getCurrentRequest()->getSchemeAndHttpHost();
    $this->config = $config_factory;
    $this->mauticCommerceApi = $mautic_paragraph_commerce_api;
    $this->queue = $queue_factory->get('mautic_paragraph_commerce_add_contact_to_segment');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // The format for adding a state machine event to subscribe to is:
    // {group}.{transition key}.pre_transition or
    // {group}.{transition key}.post_transition
    // depending on when you want to react.
    $events = [
      'commerce_order.place.post_transition' => [
        'addCustomerToMauticSegment',
        100,
      ],
    ];
    return $events;
  }

  /**
   * Add customer to Mautic segments based on order items and promotions.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function addCustomerToMauticSegment(WorkflowTransitionEvent $event) {
    $mautic_settings = $this->config->get('mautic_paragraph_commerce.settings');
    if ($mautic_settings->get('mautic_segment_disable') == TRUE) {
      return;
    }

    // Get the order entity.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $marketing_terms = $order->getData('mautic_terms_and_conditions');
    $cpms_config = $this->config->get('mautic_paragraph_commerce.commerce_promotions_mautic_segments');
    $marketing_list = [];
    $exclude_from_marketing_list = [];
    $applied_promotions = [];

    // Get list of promotions applied to this order.
    foreach ($order->collectAdjustments() as $adjustment) {
      if ($adjustment->getType() == 'promotion') {
        $applied_promotions[$adjustment->getSourceId()] = $adjustment->getLabel();
      }
    }

    // Get configuration map for commerce promotions to Mautic segments.
    if (is_array($cpms_config->get('fields_values'))) {
      $promotions_list = $cpms_config->get('fields_values');
      $selected_promo = array_intersect_key($promotions_list, $applied_promotions);
      foreach ($selected_promo as $mautic_segment) {
        if ($mautic_segment['exclude_from_marketing_terms'] == TRUE) {
          $exclude_from_marketing_list[$mautic_segment['mautic_segment']] = $mautic_segment['mautic_segment'];
        }
        else {
          $marketing_list[$mautic_segment['mautic_segment']] = $mautic_segment['mautic_segment'];
        }
      }
    }

    // Get order items.
    foreach ($order->getItems() as $order_item) {

      // Get product variation.
      $product_variation = $order_item->getPurchasedEntity();
      if ($product_variation) {
        $definitions = $product_variation->getFieldDefinitions();
        foreach ($definitions as $field_type => $definition) {
          $definition_type = $definition->getType();

          // Check if mautic_segment_list exists in product variation.
          if ($definition_type === 'mautic_segment_list') {

            $field = $product_variation->get($field_type);
            if (!$field->isEmpty()) {
              $mautic_segments = $field->getValue();

              // Get list of Mautic segments.
              if (is_array($mautic_segments)) {
                foreach ($mautic_segments as $segment) {
                  if (isset($segment['value'])) {
                    if (isset($segment['value']) && $segment['exclude_from_marketing_terms'] == TRUE) {
                      $exclude_from_marketing_list[$segment['value']] = $segment['value'];
                    }
                    else {
                      $marketing_list[$segment['value']] = $segment['value'];
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    if ($marketing_terms == TRUE) {
      $final_segment_list = array_unique(array_merge($marketing_list, $exclude_from_marketing_list), SORT_REGULAR);
    }
    else {
      $final_segment_list = $exclude_from_marketing_list;
    }
    if ($mautic_settings->get('mautic_segment_debug') == TRUE) {
      $this->logger->debug('Marketing Mautic segmnet list: <pre>@data</pre>', [
        '@data' => print_r($marketing_list, TRUE),
      ]);
      $this->logger->debug('Excluded from Marketing Mautic segmnet list: <pre>@data</pre>', [
        '@data' => print_r($exclude_from_marketing_list, TRUE),
      ]);
    }

    if (!empty($final_segment_list)) {

      // Get customer details.
      $email = $order->getEmail();
      /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
      $billing_profile = $order->getBillingProfile();
      if ($billing_profile && $billing_profile->address) {
        $address = $billing_profile->address->first();
        $first_name = $address->getGivenName();
        $last_name = $address->getFamilyName();
        $country_code = $address->getCountryCode();
        $city = $address->getLocality();
      }

      $countries = CountryManager::getStandardList();
      $full_country_name = $countries[$country_code]->__toString();

      $data = [
        'firstname' => $first_name ?? '',
        'lastname' => $last_name ?? '',
        'country' => $full_country_name ?? '',
        'city' => $city ?? '',
        'email' => $email,
        'website' => $this->host,
        'ipAddress' => $this->clientIp,
        'overwriteWithBlank' => TRUE,
      ];
      $queue_data['data'] = $data;
      $queue_data['segment_list'] = $final_segment_list;

      if ($mautic_settings->get('mautic_segment_debug') == TRUE) {
        $this->logger->debug('Contact data before Mautic contact creation: <pre>@data</pre>', [
          '@data' => print_r($queue_data['data'], TRUE),
        ]);
        $this->logger->debug('Final Mautic segment list to add contact to: <pre>@data</pre>', [
          '@data' => print_r($queue_data['segment_list'], TRUE),
        ]);
      }

      if ($mautic_settings->get('mautic_segment_queue') == TRUE) {
        // Create queue item to be processed on next cron.
        $this->queue->createItem($queue_data);
      }
      else {
        // Create Mautic contact from customer billing profile.
        $mautic_contact = $this->mauticCommerceApi->createContact($data);

        // Add contact to segments.
        foreach ($final_segment_list as $segment_id) {
          if (isset($mautic_contact['contact']['id'])) {
            if ($mautic_contact['contact']['id']) {
              $response = $this->mauticCommerceApi->addContactToSegment($segment_id, $mautic_contact['contact']['id']);
              if (isset($response['errors'])) {
                if ($response['errors']) {
                  // Get error message.
                  $error_message = explode('Response:', $response['errors']['0']['message'])[0];
                  $this->logger->warning('Could not add contact to segment with id %segment_id. %error',
                    ['%segment_id' => $segment_id, '%error' => $error_message]);
                }
              }
            }
          }
        }
      }
    }

  }

}
