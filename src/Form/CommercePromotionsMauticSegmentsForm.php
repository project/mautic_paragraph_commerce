<?php

namespace Drupal\mautic_paragraph_commerce\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure commerce promotions to Mautic segments for this site.
 */
class CommercePromotionsMauticSegmentsForm extends ConfigFormBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger to send info or warnings to Drupal with.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The MauticParagraphApi service.
   *
   * @var \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface
   */
  protected $mauticCommerceApi;

  /**
   * Constructs a new CommercePromotionsMauticSegments object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger to send info or warnings to Drupal with.
   * @param \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface $mautic_paragraph_commerce_api
   *   The Mautic API service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, MauticCommerceApiInterface $mautic_paragraph_commerce_api) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->mauticCommerceApi = $mautic_paragraph_commerce_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('mautic_paragraph_commerce_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mautic_paragraph_commerce_commerce_promotions_mautic_segments';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mautic_paragraph_commerce.commerce_promotions_mautic_segments'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['field_value_title'] = [
      '#type' => 'item',
      '#title' => $this->t('Commerce Promotions and Mautic Segments.'),
    ];

    $form['field_value'] = [
      '#type' => 'table',
      '#sticky' => TRUE,
      '#header' => [
        $this->t('Commerce Promotions'),
        $this->t('Mautic Segments'),
        $this->t('Exclude from Marketing terms'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('There are currently no values. Add one by selecting an option below.'),
    ];

    $fields_values = $this->config('mautic_paragraph_commerce.commerce_promotions_mautic_segments')->get('fields_values');

    $segments_list = [];
    try {
      if ($this->mauticCommerceApi->getStatus()) {
        $segments_list = $this->mauticCommerceApi->getSegmentList();
      }
    }
    catch (\Exception $e) {
      watchdog_exception('error', $e);
      $this->messenger->addError($this->t('Failed to retrieve Mautic segments. there was a problem with the connection to the Mautic API instance.'));
    }

    $promotions_list = $this->entityTypeManager->getStorage('commerce_promotion')->loadMultiple();
    $promotions = [];
    foreach ($promotions_list as $promotion) {
      $promotions[$promotion->id()] = $promotion->label();
    }

    $segments = [];
    foreach ($segments_list as $item) {
      $segments[$item['id']] = $item['name'];
    }
    $ajax_wrapper_id = Html::getUniqueId('ajax-wrapper');
    $form['fields_values'] = [
      '#type' => 'value',
      '#value' => $fields_values,
      '#prefix' => '<div id="' . $ajax_wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    if (is_array($fields_values)) {
      foreach ($fields_values as $key => $value) {
        $form['field_value'][$key]['#attributes'] = ['id' => 'field_value-' . $key];

        $form['field_value'][$key]['commerce_promotion'] = [
          'data' => [
            'label' => [
              '#plain_text' => $promotions[$value['commerce_promotion']],
            ],
          ],
        ];

        $form['field_value'][$key]['mautic_segment'] = [
          'data' => [
            'label' => [
              '#markup' => $segments[$value['mautic_segment']] ?? '',
            ],
          ],
        ];

        $form['field_value'][$key]['exclude_from_marketing_terms'] = [
          'data' => [
            'label' => [
              '#markup' => nl2br(($value['exclude_from_marketing_terms'] == TRUE) ? 'YES' : 'NO'),
            ],
          ],
        ];

        $links = [];

        $links['delete'] = [
          'title' => $this->t('Remove'),
          'url' => Url::fromRoute('mautic_paragraph_commerce.commerce_promotions_mautic_segments.items.remove', [
            'item_id' => $key,
          ], [
            'attributes' => ['class' => ['use-ajax']],
          ]),
        ];
        $form['field_value'][$key]['operations'] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];

      }
    }

    $form['field_value']['new'] = [
      '#tree' => FALSE,
    ];

    $form['field_value']['new']['commerce_promotion'] = [
      'data' => [
        'commerce_promotion' => [
          '#type' => 'select',
          '#title' => $this->t('Promotion'),
          '#title_display' => 'invisible',
          '#options' => $promotions,
          '#empty_option' => $this->t('Select the Promotion'),
        ],
      ],
      '#prefix' => '<div class="field-value-new">',
      '#suffix' => '</div>',
    ];

    $form['field_value']['new']['mautic_segment'] = [
      'data' => [
        'mautic_segment' => [
          '#type' => 'select',
          '#title' => $this->t('Segment'),
          '#title_display' => 'invisible',
          '#options' => $segments,
          '#empty_option' => $this->t('Select the Segment'),
        ],
      ],
      '#prefix' => '<div class="field-value-new">',
      '#suffix' => '</div>',
    ];

    $form['field_value']['new']['exclude_from_marketing_terms'] = [
      '#type' => 'checkbox',
    ];

    $form['field_value']['new']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#validate' => ['::formValidate'],
      '#submit' => ['::fieldValueSave'],
    ];

    $form = parent::buildForm($form, $form_state);
    unset($form['actions']);
    return $form;
  }

  /**
   * Map commerce promotion to Mautic segment.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function fieldValueSave(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\business_rules\Entity\Action $action */
    $field               = $form_state->getValue('commerce_promotion');
    $value               = $form_state->getValue('mautic_segment');
    $exclude             = $form_state->getValue('exclude_from_marketing_terms');
    $config              = $this->config('mautic_paragraph_commerce.commerce_promotions_mautic_segments');
    $field_value         = is_array($config->get('fields_values')) ? $config->get('fields_values') : [];
    $field_value[$field] = [
      'commerce_promotion' => $field,
      'mautic_segment'  => $value,
      'exclude_from_marketing_terms' => $exclude,
    ];

    $config->set('fields_values', $field_value);
    $config->save();

  }

  /**
   * Validate handler for commerce promotions and Mautic segments.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function formValidate(array $form, FormStateInterface $form_state) {
    if (!$form_state->getValue('commerce_promotion')) {
      $form_state->setErrorByName('field', t('Select an commerce promotion to add.'));
    }
    if (!$form_state->getValue('mautic_segment')) {
      $form_state->setErrorByName('field_value', t('Select an Mautic segment to add.'));
    }
  }

}
