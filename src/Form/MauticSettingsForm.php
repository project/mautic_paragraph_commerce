<?php

namespace Drupal\mautic_paragraph_commerce\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Mautic Paragraph Commerce settings for this site.
 */
class MauticSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mautic_paragraph_commerce_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mautic_paragraph_commerce.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mautic_paragraph_commerce.settings');

    $form['mautic_segment_disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Mautic Segments'),
      '#description' => $this->t('Contacts will not be created or added to segments when this option is checked.'),
      '#default_value' => $config->get('mautic_segment_disable') ?? FALSE,
    ];
    $form['mautic_segment_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Trigger API during cron instead of commerce order event'),
      '#description' => $this->t('This option is highly recommended, contacts will be added to a queue and will be created and added to segments during cron.'),
      '#default_value' => $config->get('mautic_segment_queue') ?? TRUE,
    ];

    $form['mautic_segment_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug Mautic segments'),
      '#description' => $this->t('Log in watchdog all Mautic segment IDs from commerce variation and commerce promotions.'),
      '#default_value' => $config->get('mautic_segment_debug') ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mautic_paragraph_commerce.settings')
      ->set('mautic_segment_disable', $form_state->getValue('mautic_segment_disable'))
      ->set('mautic_segment_queue', $form_state->getValue('mautic_segment_queue'))
      ->set('mautic_segment_debug', $form_state->getValue('mautic_segment_debug'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
