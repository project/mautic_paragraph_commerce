<?php

namespace Drupal\mautic_paragraph_commerce\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'autocomplete_mautic_segments' widget.
 *
 * @FieldWidget(
 *   id = "autocomplete_mautic_segments",
 *   label = @Translation("Autocomplete mautic segment list"),
 *   field_types = {
 *     "mautic_segment_list"
 *   }
 * )
 */
class AutocompleteSegmentsWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The messenger to send info or warnings to Drupal with.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Mautic Api.
   *
   * @var \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface
   */
  protected $mauticCommerceApi;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger to send info or warnings to Drupal with.
   * @param \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface $mautic_paragraph_commerce_api
   *   The Mautic Api.
   */
  public function __construct($plugin_id,
    $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings,
                              array $third_party_settings,
                              MessengerInterface $messenger,
                              MauticCommerceApiInterface $mautic_paragraph_commerce_api) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->messenger = $messenger;
    $this->mauticCommerceApi = $mautic_paragraph_commerce_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'],
      $container->get('messenger'),
      $container->get('mautic_paragraph_commerce_api'));
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $value = $items[$delta]->value ?? '';
    $values = $items[$delta]->getValue();
    $title = '';
    $disable_element = FALSE;
    try {
      if ($this->mauticCommerceApi->getStatus()) {
        if ($value !== '') {
          $title = $this->mauticCommerceApi->getSegmentTitle($value);
        }
      }
    }
    catch (\Exception $e) {
      watchdog_exception('error', $e);
      $element['status_details'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Status'),
        '#title_display' => 'before',
        '#open' => TRUE,
        '#description' => $this->t('Connection failed, there was a problem with the connection to the Mautic API instance.'),
      ];
      $this->messenger->addError($this->t('Failed to retrieve Mautic segments. there was a problem with the connection to the Mautic API instance.'));
      $disable_element = TRUE;
    }

    $element += [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#open' => TRUE,
    ];

    $element['value'] = [
      '#type' => 'textfield',
      '#default_value' => $value === '' ? '' : $title . ' (' . $value . ')',
      '#autocomplete_route_name' => 'mautic_paragraph_commerce.autocomplete.segments',
      '#disabled' => $disable_element,
    ];

    $element['exclude_from_marketing_terms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude segment from marketing terms on customer checkout.'),
      '#default_value' => $values['exclude_from_marketing_terms'] ?? 0,
      '#disabled' => $disable_element,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      $values[$key]['value'] = EntityAutocomplete::extractEntityIdFromAutocompleteInput($value['value']);
      $values[$key]['exclude_from_marketing_terms'] = $value['exclude_from_marketing_terms'];
    }
    return parent::massageFormValues($values, $form, $form_state);
  }

}
