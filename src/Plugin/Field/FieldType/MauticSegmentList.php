<?php

namespace Drupal\mautic_paragraph_commerce\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;

/**
 * Plugin implementation of the 'mautic_segment_list' field type.
 *
 * @FieldType(
 *   id = "mautic_segment_list",
 *   label = @Translation("Mautic segment list"),
 *   description = @Translation("Display Mautic available segments."),
 *   category = @Translation("Reference"),
 *   default_widget = "autocomplete_mautic_segments",
 *   default_formatter = "list_default",
 * )
 */
class MauticSegmentList extends ListIntegerItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'allowed_values' => [],
      'allowed_values_function' => 'mautic_paragraph_commerce_segment_list',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t('Integer value'))
      ->setRequired(TRUE);

    $properties['exclude_from_marketing_terms'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Exclude segment from marketing terms on customer checkout.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
        ],
        'exclude_from_marketing_terms' => [
          'type' => 'int',
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

}
