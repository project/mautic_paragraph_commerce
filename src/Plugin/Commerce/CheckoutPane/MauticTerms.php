<?php

namespace Drupal\mautic_paragraph_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Mautic marketing terms and conditions.
 *
 * @CommerceCheckoutPane(
 *   id = "mautic_terms",
 *   label = @Translation("Agree to Mautic marketing terms and conditions"),
 *   default_step = "_disabled",
 * )
 */
class MauticTerms extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'nid' => NULL,
      'link_text' => 'Marketing terms and conditions',
      'link_url' => '',
      'prefix_text' => 'We may send you other suitable products, services, or relevant information by email, %terms. <br> Untick the box to opt out.',
      'mautic_terms_and_conditions' => FALSE,
      'new_window' => 1,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $prefix = $this->configuration['prefix_text'];
    $new_window = $this->configuration['new_window'];
    $summary = '';
    if (!empty($prefix)) {
      $summary = $this->t('Prefix text: @text', ['@text' => $prefix]) . '<br/>';
    }
    if (!empty($new_window)) {
      $window_text = ($new_window === 1) ? $this->t('New window') : $this->t('Same window');
      $summary .= $this->t('Window opens in: @text', ['@text' => $window_text]) . '<br/>';
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['prefix_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prefix text'),
      '#description' => $this->t('Optional include the word %terms to add a link to marketing terms and conditions'),
      '#default_value' => $this->configuration['prefix_text'],
    ];
    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $this->configuration['link_text'],
    ];
    $form['link_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Link url'),
      '#description' => $this->t('Example: https://example.com'),
      '#default_value' => $this->configuration['link_url'],
      '#maxlength' => 255,
      '#size' => 30,
    ];
    $form['new_window'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open window link in new window'),
      '#default_value' => $this->configuration['new_window'],
    ];

    $form['mautic_terms_and_conditions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default value for terms and condition checkbox'),
      '#description' => $this->t('Check this if you want customer to Opt-out or un-check if you
                                        want customers to Opt-in'),
      '#default_value' => $this->configuration['mautic_terms_and_conditions'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['prefix_text'] = Xss::filterAdmin($values['prefix_text']);
      $this->configuration['link_text'] = Xss::filter($values['link_text']);
      $this->configuration['link_url'] = $values['link_url'];
      $this->configuration['new_window'] = $values['new_window'];
      $this->configuration['mautic_terms_and_conditions'] = $values['mautic_terms_and_conditions'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $prefix_text = $this->configuration['prefix_text'];
    $link_text = $this->configuration['link_text'];
    $link_url = $this->configuration['link_url'];
    $attributes = [];
    if ($this->configuration['new_window']) {
      $attributes = ['attributes' => ['target' => '_blank']];
    }
    if ($link_url) {
      $url = Url::fromUri($link_url)->setOptions($attributes);
      $link = Link::fromTextAndUrl($link_text, $url)->toString();
      $pane_form['mautic_terms_and_conditions'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->configuration['mautic_terms_and_conditions'],
        '#title' => $this->t($prefix_text, ['%terms' => $link]),
        '#required' => FALSE,
        '#weight' => $this->getWeight(),
        '#attributes' => [
          'class' => ['mautic-paragraph-commerce marketing-terms'],
        ]
      ];
    }
    else {
      $pane_form['mautic_terms_and_conditions'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->configuration['mautic_terms_and_conditions'],
        '#title' => $this->t($prefix_text),
        '#required' => FALSE,
        '#weight' => $this->getWeight(),
        '#attributes' => [
          'class' => ['mautic-paragraph-commerce marketing-terms'],
        ]
      ];
    }
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    $this->order->setData('mautic_terms_and_conditions', $values['mautic_terms_and_conditions']);
  }

}
