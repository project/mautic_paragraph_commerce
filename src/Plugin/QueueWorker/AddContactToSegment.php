<?php

namespace Drupal\mautic_paragraph_commerce\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add queued contacts to Mautic segments.
 *
 * @QueueWorker(
 *   id = "mautic_paragraph_commerce_add_contact_to_segment",
 *   title = @Translation("Add contact to Mautic Segment."),
 *   cron = {"time" = 30}
 * )
 */
class AddContactToSegment extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates a new WishlistItemDelete object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   LoggerInterface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.mautic_paragraph_commerce')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\mautic_paragraph_commerce\MauticCommerceApiInterface $mautic_api */
    $mautic_api = \Drupal::service('mautic_paragraph_commerce_api');

    if (isset($data['data'])) {
      if ($data['data']) {
        try {
          if ($mautic_api->getStatus()) {
            $mautic_contact = $mautic_api->createContact($data['data']);
            if (isset($mautic_contact['errors'])) {
              if ($mautic_contact['errors']) {
                $error_message = explode('Response:', $mautic_contact['errors']['0']['message'])[0];
                // Get error message.
                $this->logger->warning('Could not create contact. %error',
                  ['%error' => $error_message]);
              }
            }
          }
          if (isset($data['segment_list']) && isset($mautic_contact['contact']['id'])) {
            if ($data['segment_list']) {
              foreach ($data['segment_list'] as $segment_id) {
                $response = $mautic_api->addContactToSegment($segment_id, $mautic_contact['contact']['id']);
                if (isset($response['errors'])) {
                  if ($response['errors']) {
                    $error_message = explode('Response:', $response['errors']['0']['message'])[0];
                    // Get error message.
                    $this->logger->warning('Could not add contact to segment with id %segment_id. %error',
                      ['%segment_id' => $segment_id, '%error' => $error_message]);
                  }
                }
                else {
                  $this->logger->notice('Add queued contact with email %email to Mautic segment %segment_id.
              Mautic contact id %contact_id.',
                    [
                      '%segment_id' => $segment_id,
                      '%email' => $data['data']['email'],
                      '%contact_id' => $mautic_contact['contact']['id'],
                    ]);
                }
              }
            }
          }
        }
        catch (\Exception $e) {
          watchdog_exception('error', $e);
          $this->logger->error('Connection failed, there was a problem with the connection to the Mautic API instance.');
        }
      }
    }

  }

}
