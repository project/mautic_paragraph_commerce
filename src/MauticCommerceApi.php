<?php

namespace Drupal\mautic_paragraph_commerce;

use Drupal\mautic_paragraph\MauticParagraphApi;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Mautic\Exception\ContextNotFoundException;
use Mautic\Exception\RequiredParameterMissingException;
use Mautic\MauticApi;

/**
 * Mautic Integration service.
 */
class MauticCommerceApi extends MauticParagraphApi implements MauticCommerceApiInterface {

  /**
   * {@inheritdoc}
   */
  public function createContact($data) {
    if ($this->getApiClient() != NULL) {

      // Get base url from config.
      $url = $this->getServerUri();
      $auth = $this->getApiClient();

      // Make new mauticAPI instance.
      $api = new MauticApi();

      // Get formsAPI.
      $contact_api = $api->newApi('contacts', $auth, $url);
      $contact = $contact_api->create($data);
      return $contact;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function addContactToSegment($segment_id, $contact_id) {
    if ($this->getApiClient() != NULL) {
      // Get base url from config.
      $url = $this->getServerUri();
      $auth = $this->getApiClient();

      // Make new mauticAPI instance.
      $api = new MauticApi();

      // Get formsAPI.
      $segment_api = $api->newApi('segments', $auth, $url);
      $response = $segment_api->addContact($segment_id, $contact_id);
      return $response;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentList($input = NULL) {

    if ($this->getApiClient() != NULL) {

      $cache = $this->cacheBackend->get('mautic_segment_list');
      $data = $cache->data ?? [];

      // If no cached data, fetch data from API.
      if (!$data) {
        try {
          // Get base url from config.
          $url = $this->getServerUri();
          $auth = $this->getApiClient();

          // Make new mauticAPI instance.
          $api = new MauticApi();

          // Get formsAPI.
          $segment_api = $api->newApi('segments', $auth, $url);

          // Cache the full list when it's not currently cached.
          $response = $segment_api->getList('', 0, 100, '', 'ASC', TRUE, TRUE);

          // Check for errors.
          if (isset($response['errors'])) {
            if ($response['errors']) {
              // Get error message.
              $error_message = explode('Response:', $response['errors']['0']['message'])[0];

              // Check if unauthorized error code, throws another exception.
              if ($response['errors']['0']['code'] == 401) {
                throw new UnauthorizedHttpException(' Basic realm="Mautic integration module', $error_message);
              }
              throw new \Exception($error_message);
            }
          }
          $this->cacheBackend->set('mautic_segment_list', $response['lists'], time() + 3600, [
            'config:mautic_paragraph_commerce.settings',
          ]);
          $data = $response['lists'];
        }
        catch (RequiredParameterMissingException $e) {
          watchdog_exception('error', $e);
          // Display credentials error message.
          // Get link from route settings page.
          $link = Url::fromRoute('mautic_paragraph.route_settings');
          // Make the link.
          $link_message = Link::fromTextAndUrl($this->t('mautic settings page'), $link)
            ->toString();
          // Display error.
          $this->messenger->addError($this->t('Failed to retrieve forms, missing credentials. Check %link .', ['%link' => $link_message]));
        }
        catch (ContextNotFoundException $e) {
          watchdog_exception('error', $e);
          $this->messenger->addError($this->t('Failed to retrieve forms, invalid Context.'));
        }
        catch (UnauthorizedHttpException $e) {
          watchdog_exception('error', $e);
          $this->messenger->addError($this->t('Failed to retrieve forms. Your credentials in the Mautic Integration settings page are wrong.'));
        }
        catch (\Exception $e) {
          watchdog_exception('error', $e);
          $this->messenger->addError($this->t('Failed to retrieve forms. Verify your settings on the Mautic paragraph Integration settings page.'));
        }
      }
      if ($input) {
        $data = array_filter($data, function ($k) use (&$input) {
          return strpos(strtolower($k['name']), strtolower($input)) !== FALSE;
        });
        return $data;
      }
      return $data;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentTitle($id) {
    if ($this->getApiClient() != NULL) {
      // Get base url from config.
      $url = $this->getServerUri();
      $auth = $this->getApiClient();

      // Make new mauticAPI instance.
      $api = new MauticApi();

      // Get formsAPI.
      $segment_api = $api->newApi('segments', $auth, $url);
      $segments_list = $segment_api->getList('', 0, 100, '', 'ASC', TRUE, TRUE);

      // Loop over $segments_list to get name of matching form.
      $segment = array_filter($segments_list['lists'], function ($item) use (&$id) {
        return $item['id'] == $id;
      });

      // When there is no match, return null.
      if (count($segment) === 0) {
        return NULL;
      }
      return array_shift($segment)['name'];
    }
    return NULL;
  }

}
