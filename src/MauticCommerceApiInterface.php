<?php

namespace Drupal\mautic_paragraph_commerce;

use Drupal\mautic_paragraph\MauticParagraphApiInterface;

/**
 * Interface for MauticCommerceApi service.
 */
interface MauticCommerceApiInterface extends MauticParagraphApiInterface {

  /**
   * Create a contact in Mautic.
   *
   * @param array $data
   *   Contact data array.
   *
   * @return array
   *   Mautic response.
   */
  public function createContact(array $data);

  /**
   * Create a contact in Mautic.
   *
   * @param string $segment_id
   *   The segment id.
   * @param string $contact_id
   *   The contact id.
   *
   * @return array
   *   Mautic response.
   */
  public function addContactToSegment($segment_id, $contact_id);

  /**
   * Provides a list of segments.
   *
   * @param string $input
   *   Search string for fetching segments.
   *
   * @return array
   *   List of segments from Mautic.
   */
  public function getSegmentList($input = NULL);

  /**
   * Fetches the segment title from an id.
   *
   * @param int $id
   *   The form id.
   *
   * @return string
   *   Name of the form.
   */
  public function getSegmentTitle($id);

}
