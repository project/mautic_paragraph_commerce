<?php

namespace Drupal\mautic_paragraph_commerce\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mautic_paragraph_commerce\MauticCommerceApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class SegmentAutocompleteController extends ControllerBase {

  /**
   * The MauticParagraphApi service.
   *
   * @var \Drupal\mautic_paragraph_commerce\MauticCommerceApi
   */
  protected $mauticCommerceApi;

  /**
   * Construct SegmentAutoCompleteController class.
   *
   * @param \Drupal\mautic_paragraph_commerce\MauticCommerceApi $mautic_paragraph_commerce_api
   *   The Mautic Commerce API service.
   */
  public function __construct(MauticCommerceApi $mautic_paragraph_commerce_api) {
    $this->mauticCommerceApi = $mautic_paragraph_commerce_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('mautic_paragraph_commerce_api')
    );
  }

  /**
   * Handler for autocomplete request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return formlist as Json.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    try {
      if ($this->mauticCommerceApi->getStatus()) {
        $segments_list = $this->mauticCommerceApi->getSegmentList($input);
      }
    }
    catch (\Exception $e) {
      watchdog_exception('error', $e);
    }

    $results = [];
    foreach ($segments_list as $item) {
      array_push($results, [
        'label' => $item['name'],
        'value' => $item['name'] . ' (' . $item['id'] . ')',
      ]);
    }
    return new JsonResponse($results);
  }

}
