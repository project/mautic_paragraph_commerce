<?php

namespace Drupal\mautic_paragraph_commerce\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Mautic Paragraph Commerce routes.
 */
class PromotionSegmentController extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Remove item from condition.
   *
   * @param string $item_id
   *   The item id.
   */
  public function removeItem($item_id) {
    $config = $this->configFactory->getEditable('mautic_paragraph_commerce.commerce_promotions_mautic_segments');
    $items = $config->get('fields_values');
    unset($items[$item_id]);
    $items = is_null($items) ? [] : $items;

    $config->set('fields_values', $items);
    $config->save();

    $response = new AjaxResponse();
    $response->addCommand(new RemoveCommand('#field_value-' . $item_id));

    return $response;

  }

}
