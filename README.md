CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

 * For a full description of the module visit:
   https://www.drupal.org/project/mautic_paragraph_commerce

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/mautic_paragraph_commerce


REQUIREMENTS
------------

You will need to have the
<a href="https://www.drupal.org/project/mautic_paragraph">mautic_paragraph</a> and
<a href="https://www.drupal.org/project/commerce">commerce</a>
module installed and configured.


INSTALLATION
------------

Install the mautic_paragraph_commerce module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Web Services > Mautic paragraph
3. Choose Oauth and configure Mautic connector
4. (Optional) Navigate to Administration > Web Services > Mautic paragraph > Mautic settings
5. (Optional) Enable Debug Mautic segments
6. Navigate to Administration > Commerce > Configuration > Checkout Flows > [ACTIVE CHECKOUT FLOW]
7. Drag "Agree to Mautic marketing terms and conditions" to Review checkout step.


USAGE
-------------
1. Navigate to Administration > Commerce > Promotions > Mautic Segments
2. (Optional) Map Promotions to Mautic Segments
3. Navigate to Administration > Commerce > Configuration > Product Variation Types > [MY EXAMPLE VARIATION NAME]
4. On Manage fields click Add field
5. Select Mautic segment list field type from Reference
6. On Manage form display the widget for the Mautic segment list field type should be "Autocomplete mautic segment list"
7. Navigate to Administration > Commerce > Products
8. Edit product variation and reference existing Mautic segments

IMPORTANT NOTES
---------------
Mautic Segments can be excluded from the checkbox "Marketing terms" on customer checkout by enabling
"Exclude segment from marketing terms on customer checkout." on product variations or by enabling
"Exclude from Marketing terms" on Promotions Tab > Mautic Segments.

The contact created in Mautic will contain the following data:
<code>
$data = [
'firstname' => $first_name ?? '',
'lastname' => $last_name ?? '',
'country' => $full_country_name ?? '',
'city' => $city ?? '',
'email' => $email,
'website' => \Drupal::request()->getSchemeAndHttpHost(),
'ipAddress' => $this->clientIp,
'overwriteWithBlank' => true,
];
</code>

MAINTAINERS
-----------

 * lexsoft - https://www.drupal.org/u/lexsoft
